RSpec.describe 'Qabotfy commands ACs' do
  around :each do |e|
    VCR.turned_off { e.run }
  end

  before do
    WebMock.disable_net_connect!
  end

  let(:all_taxis_return_body) { File.read('spec/json/taxis.json') }
  let(:hire_request_body) { File.read('spec/json/hire_taxi.json') }

  it 'should response with the nearest Taxi hired' do
    hired_return_body = File.read('spec/json/hired_Opel.json')

    stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: all_taxis_return_body)

    stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Opel').
        with(:body => hire_request_body).
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: hired_return_body)

    expected_responses = ["Trying to request the nearest taxi for Anonymous","A Opel is hired for Anonymous"]
    expect(message: 'Taxi request for 38.88 lat and 1.399 lon', channel: 'channel').
        to respond_with_slack_messages(expected_responses)
  end

  it 'should response with the nearest Taxi hired other coordinates' do
    hired_return_body = File.read('spec/json/hired_hyundai.json')

    stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: all_taxis_return_body)

    stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Hyundai').
        with(:body => hire_request_body).
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: hired_return_body)

    expected_responses = ["Trying to request the nearest taxi for Anonymous","A Hyundai is hired for Anonymous"]
    expect(message: 'Taxi request for 38.41 lat and 1.2313 lon', channel: 'channel').
        to respond_with_slack_messages(expected_responses)
  end

  it 'should response with the next nearest Taxi if the first one it is hired' do
    hired_return_body = File.read('spec/json/hired_Hyundai.json')

    stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: all_taxis_return_body)

    stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Opel').
        with(:body => hire_request_body).
        to_return(:status => 403)

    stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Hyundai').
        with(:body => hire_request_body).
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: hired_return_body)

    expected_responses = ["Trying to request the nearest taxi for Anonymous","A Hyundai is hired for Anonymous"]
    expect(message: 'Taxi request for 38.88 lat and 1.399 lon', channel: 'channel').
        to respond_with_slack_messages(expected_responses)
  end

  it 'should response sorry there are not available taxis if all taxis are hired after calling all taxis' do
    stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: all_taxis_return_body)

    stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Opel').
        with(:body => hire_request_body).
        to_return(:status => 403)

    stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Hyundai').
        with(:body => hire_request_body).
        to_return(:status => 403)

    stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Skoda4').
        with(:body => hire_request_body).
        to_return(:status => 403)

    expected_responses = ["Trying to request the nearest taxi for Anonymous","Sorry Anonymous, there are not available taxis :("]
    expect(message: 'Taxi request for 38.88 lat and 1.399 lon', channel: 'channel').
        to respond_with_slack_messages(expected_responses)
  end

  it 'should response sorry there are not available taxis, if all taxis are hired before calling all taxis' do
    no_taxis_free_return_body = File.read('spec/json/no_taxis_free.json')

    stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: no_taxis_free_return_body)


    expected_responses = ["Trying to request the nearest taxi for Anonymous","Sorry Anonymous, there are not available taxis :("]
    expect(message: 'Taxi request for 38.88 lat and 1.399 lon', channel: 'channel').
        to respond_with_slack_messages(expected_responses)
  end

  it 'should response sorry there are not available taxis, if there are no taxis' do
    stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: "[]")


    expected_responses = ["Trying to request the nearest taxi for Anonymous","Sorry Anonymous, there are not available taxis :("]
    expect(message: 'Taxi request for 38.88 lat and 1.399 lon', channel: 'channel').
        to respond_with_slack_messages(expected_responses)
  end

  it 'should response range of latitude' do
    expected_response = "lat must be between -90 and 90"
    expect(message: 'Taxi request for 96.88 lat and 1.399 lon', channel: 'channel').
        to respond_with_slack_message(expected_response)
  end

  it 'should response range of longitude' do
    expected_response = "lon must be between -180 and 180"
    expect(message: 'Taxi request for 6.88 lat and 181.399 lon', channel: 'channel').
        to respond_with_slack_message(expected_response)
  end

  it 'should not response latitude and longitude because must be numeric' do
    expect(message: 'Taxi request for aaa lat and ss lon', channel: 'channel').
        to not_respond
  end

  it 'should response sorry Something went wrong if there are a problem with the server(all taxi)' do
    stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
        to_return(:status => 500)


    expected_responses = ["Trying to request the nearest taxi for Anonymous","Sorry Anonymous, Something went wrong"]
    expect(message: 'Taxi request for 38.88 lat and 1.399 lon', channel: 'channel').
        to respond_with_slack_messages(expected_responses)
  end

  it 'should response sorry Something went wrong if there are a problem with the server(hire taxi)' do
    all_taxis_return_body = File.read('spec/json/taxis.json')

    stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
        to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: all_taxis_return_body)

    stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Opel').
        to_return(:status => 500)

    expected_responses = ["Trying to request the nearest taxi for Anonymous","Sorry Anonymous, Something went wrong"]
    expect(message: 'Taxi request for 38.88 lat and 1.399 lon', channel: 'channel').
        to respond_with_slack_messages(expected_responses)
  end

  it 'should response sorry Something went wrong if there are a problem with the server(timeout)' do
    stub_request(:any,/.*/).
        to_timeout

    expected_responses = ["Trying to request the nearest taxi for Anonymous","Sorry Anonymous, Something went wrong"]
    expect(message: 'Taxi request for 38.88 lat and 1.399 lon', channel: 'channel').
        to respond_with_slack_messages(expected_responses)
  end
end