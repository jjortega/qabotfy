require 'rspec'
require 'webmock/rspec'

RSpec.describe TaxiRequest do
    it 'should return the nearest taxi' do
      client = double
      all_taxis = JSON.parse(File.read('spec/json/taxis.json'))
      opel_taxi = JSON.parse(File.read('spec/json/free_Opel.json'))

      allow(client).to receive(:all_taxis) { all_taxis }
      allow(client).to receive(:hire_taxi) { opel_taxi }

      expect(TaxiRequest.new(client).ask(38.88,1.399)["name"]).to eql("Opel")
    end

    it 'should return the next nearest taxi if nearest is hired' do
      client = double
      all_taxis = JSON.parse(File.read('spec/json/taxis.json'))
      hyundai_taxi = JSON.parse(File.read('spec/json/free_hyundai.json'))

      allow(client).to receive(:all_taxis) { all_taxis }

      allow(client).to receive(:hire_taxi).with("Madrid","Opel").and_raise(QabyfyApi::HiredError)
      allow(client).to receive(:hire_taxi).with("Madrid","Hyundai") { hyundai_taxi }

      expect(client).to receive(:hire_taxi).twice
      expect(TaxiRequest.new(client).ask(38.88,1.399)["name"]).to eql("Hyundai")
    end

    it 'should return the next nearest taxi if nearest is hired and only ask for free taxis' do
      client = double
      all_taxis_not_all_free = JSON.parse(File.read('spec/json/taxis_not_all_free.json'))
      skoda_taxi = JSON.parse(File.read('spec/json/hired_skoda4.json'))

      allow(client).to receive(:all_taxis) { all_taxis_not_all_free }

      allow(client).to receive(:hire_taxi).with("Madrid","Opel").and_raise(QabyfyApi::HiredError)
      allow(client).to receive(:hire_taxi).with("Madrid","Skoda4") { skoda_taxi }

      expect(client).to receive(:hire_taxi).twice
      expect(TaxiRequest.new(client).ask(38.88,1.399)["name"]).to eql("Skoda4")
    end

    it 'should raise a UnKnownError if there are a problem a request(all taxis)' do
      client = double

      allow(client).to receive(:all_taxis).and_raise(QabyfyApi::UnknownError)

      expect { TaxiRequest.new(client).ask(38.88,1.399) }.to raise_error(QabyfyApi::UnknownError)
    end

    it 'should raise a UnKnownError if there are a problem a request(hire taxi)' do
      client = double
      all_taxis = JSON.parse(File.read('spec/json/taxis.json'))

      allow(client).to receive(:all_taxis) { all_taxis }
      allow(client).to receive(:hire_taxi).and_raise(QabyfyApi::UnknownError)

      expect { TaxiRequest.new(client).ask(38.88,1.399) }.to raise_error(QabyfyApi::UnknownError)
    end

    it 'should raise NoAvailableTaxisError if all taxis are hired when calling all_taxis' do
      client = double
      no_taxis_free = JSON.parse(File.read('spec/json/no_taxis_free.json'))

      allow(client).to receive(:all_taxis) { no_taxis_free }

      expect{ TaxiRequest.new(client).ask(38.88,1.399)["name"] }.to raise_error(NoAvailableTaxisError)
    end

    it 'should raise NoAvailableTaxisError if there are no free taxis after calling all_taxis' do
      client = double
      all_taxis = JSON.parse(File.read('spec/json/taxis.json'))

      allow(client).to receive(:all_taxis) { all_taxis }

      allow(client).to receive(:hire_taxi).and_raise(QabyfyApi::HiredError)

      expect(client).to receive(:hire_taxi).exactly(3)
      expect { TaxiRequest.new(client).ask(38.88,1.399) }.to raise_error(NoAvailableTaxisError)
    end
end