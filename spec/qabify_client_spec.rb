RSpec.describe QabyfyApi::Client do
  context 'Server is ok!' do
    it 'should receive all taxis' do
      return_body = File.read('spec/json/taxis.json')

      stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
          to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: return_body)
      expect(QabyfyApi::Client.new.all_taxis).to eql(JSON.parse(return_body))
    end

    it 'should receive all taxis in a city' do
      return_body = File.read('spec/json/taxis_in_city.json')

      stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/Madrid').
          to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: return_body)
      expect(QabyfyApi::Client.new.all_taxis_of_a_city("Madrid")).to eql(JSON.parse(return_body))
    end

    it 'should hire an specific taxi' do
      request_body = File.read('spec/json/hire_taxi.json')
      return_body = File.read('spec/json/hired_Opel.json')

      stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Opel').
          with(:body => request_body).
          to_return(:status => 200, headers: {"content-type": "application/json; charset=utf-8"}, body: return_body)
      expect(QabyfyApi::Client.new.hire_taxi("Madrid","Opel")).to eql(JSON.parse(return_body))
    end

    it 'should get HiredError taxi is already hired' do
      request_body = File.read('spec/json/hire_taxi.json')

      stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Opel').
          with(:body => request_body).
          to_return(:status => 403)
      expect{ QabyfyApi::Client.new.hire_taxi("Madrid","Opel") }.to raise_error(QabyfyApi::HiredError)
    end
  end

  context 'Server is nok!' do
    it 'should raise an unknownError exception getting all taxis' do
      stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/').
          to_return(:status => 500)
      expect { QabyfyApi::Client.new.all_taxis() }.to raise_error(QabyfyApi::UnknownError)
    end

    it 'should raise an unknownError exception getting all taxis in a city' do
      stub_request(:get, 'http://130.211.103.134:4000/api/v1/taxis/Madrid').
          to_return(:status => 500)
      expect { QabyfyApi::Client.new.all_taxis_of_a_city("Madrid") }.to raise_error(QabyfyApi::UnknownError)
    end

    it 'should raise UnknownError trying to hire a taxi' do
      request_body = File.read('spec/json/hire_taxi.json')

      stub_request(:post, 'http://130.211.103.134:4000/api/v1/taxis/Madrid/Opel').
          with(:body => request_body).
          to_return(:status => 500)
      expect { QabyfyApi::Client.new.hire_taxi("Madrid","Opel") }.to raise_error(QabyfyApi::UnknownError)
    end

    it 'should raise UnknownError when net timeout' do
      stub_request(:any,/.*/).
          to_timeout

      expect { QabyfyApi::Client.new.all_taxis() }.to raise_error(QabyfyApi::UnknownError)
      expect { QabyfyApi::Client.new.all_taxis_of_a_city("Madrid") }.to raise_error(QabyfyApi::UnknownError)
      expect { QabyfyApi::Client.new.hire_taxi("Madrid","Opel") }.to raise_error(QabyfyApi::UnknownError)
    end
  end

end