require 'Celluloid'
require 'slack-ruby-bot'
require 'httparty'
require 'addressable/template'
require 'Haversine'

require 'qabotfy/qabotfy'

require 'qabotfy/http_client/requests/taxi'
require 'qabotfy/http_client/qabify_client'

require 'qabotfy/taxi_request'