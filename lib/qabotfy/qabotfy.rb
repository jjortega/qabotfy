class Qabotfy < SlackRubyBot::Bot

end

class RequestTaxiCommand < SlackRubyBot::Commands::Base
  match /^Taxi request for (?<lat>[-+]?[0-9]*\.?[0-9]*) lat and (?<lon>[-+]?[0-9]*\.?[0-9]*) lon$/ do |client, data, match|
    user_code = data&.fetch("user")
    user_name = client&.users&.fetch(user_code)&.name ? client.users[user_code].name : "Anonymous"
    lat = match[:lat].to_f
    lon = match[:lon].to_f
    if lat < -90 or lat > 90
      client.say(channel: data.channel, text: "lat must be between -90 and 90")
      next
    end
    if lon < -180 or lon > 180
      client.say(channel: data.channel, text: "lon must be between -180 and 180")
      next
    end
    client.say(channel: data.channel, text: "Trying to request the nearest taxi for #{user_name}")
    taxi_request = TaxiRequest.new
    taxi_request_future = taxi_request.future.ask(lat,lon)
    begin
      client.say(channel: data.channel, text: "A #{taxi_request_future.value["name"]} is hired for #{user_name}")
    rescue NoAvailableTaxisError
      client.say(channel: data.channel, text: "Sorry #{user_name}, there are not available taxis :(")
    rescue QabyfyApi::UnknownError
      client.say(channel: data.channel, text: "Sorry #{user_name}, Something went wrong")
    end
  end
end