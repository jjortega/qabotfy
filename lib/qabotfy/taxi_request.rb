class TaxiRequest
  include Celluloid

  def initialize(client = QabyfyApi::Client.new)
    Celluloid.logger = nil
    @client = client
  end

  def ask(user_latitude, user_longitude)
    begin
      nearest_taxi = _nearest_taxi(user_latitude,user_longitude)
      @client.hire_taxi(nearest_taxi["city"],nearest_taxi["name"])
    rescue QabyfyApi::HiredError
      retry
    end
  end

  private

  def _nearest_taxi(user_latitude, user_longitude)
    user_position = [user_latitude, user_longitude]
    @taxis ||= all_free_taxis
    nearest_taxi = @taxis.min_by do |taxi|
      taxi_location = [taxi["location"]["lat"], taxi["location"]["lon"]]
      Haversine.distance(user_position, taxi_location).to_m
    end
    raise NoAvailableTaxisError if nearest_taxi.nil?
    @taxis.delete(nearest_taxi)
    nearest_taxi
  end

  def all_free_taxis
    @client.all_taxis.select { |taxi| taxi["state"] == "free" }
  end
end

class NoAvailableTaxisError < StandardError
end