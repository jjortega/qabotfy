module QabyfyApi
  class Client
    module Taxi
      def all_taxis
        response = perform_request(:get,"v1/taxis/")
        if response.code == 200
          response.parsed_response
        else
          raise UnknownError.new
        end
      end

      def all_taxis_of_a_city(city)
        response = perform_request(:get,"v1/taxis/#{city}")
        if response.code == 200
          response.parsed_response
        else
          raise UnknownError.new
        end
      end

      def hire_taxi(city,taxi_name)
        request_data = {}
        request_data[:body] = {state: "hired"}.to_json
        request_data[:headers] = {"Content-Type": "application/json"}
        response = perform_request(:post,"v1/taxis/#{city}/#{taxi_name}",request_data)
        if response.code == 200
          response.parsed_response
        elsif response.code == 403
          raise HiredError.new
        else
          raise UnknownError.new
        end
      end
    end
  end

  class HiredError < StandardError
  end

  class UnknownError < StandardError
  end
end