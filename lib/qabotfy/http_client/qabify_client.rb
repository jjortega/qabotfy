module QabyfyApi
  class Client
    include QabyfyApi::Client::Taxi

    def initialize(base_url = "http://130.211.103.134:4000/api/")
      @base_url = base_url
    end

    private

    def perform_request(method,path,**params)
      template = Addressable::Template.new("#{@base_url}#{path}{?query*}")
      uri = template.expand(params)
      begin
        HTTParty.send(method,uri.to_s,params)
      rescue StandardError
        raise UnknownError
      end
    end
  end
end

