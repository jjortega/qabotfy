# Qabotfy

## Installation
   
   `bundle install`

## Usage

   `SLACK_API_TOKEN=<token in ultra_secret_token file> bundle exec bin/start` to init bot
   
### Command supported

    Taxi request for <lat> lat and <lon> lon

## Test

   `bundle exec rspec`
   
   * command_spec.rb -> ACs with mock BE
   * qabify_client_spec -> Unit test just for httpClient class
   * taxi_resquest_spec -> Unit test just for taxi_request class
   
   
## Extra gif

![](https://media.giphy.com/media/3rKFa3sCvKBL7aDlN6/giphy.gif)